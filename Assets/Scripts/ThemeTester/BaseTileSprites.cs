﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseTileSprites {

	public List<Sprite> defaultSprites = new List<Sprite>();
	public List<Sprite>[] colorSprites = new List<Sprite>[7]; //6 regular tiles + wind
	public List<Sprite>[] shapeSprites = new List<Sprite>[7]; 
	
	public BaseTileSprites()
	{
		for(int i =0; i < 7; ++i)
		{
			colorSprites[i] = new List<Sprite>();
			shapeSprites[i] = new List<Sprite>();
		}
	}
}
