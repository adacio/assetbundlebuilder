using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

#if UNITY_CLOUD_BUILD
public class WebGLCloudBuildSettings : ScriptableObject
{

    public static void PreExport(UnityEngine.CloudBuild.BuildManifestObject manifest)
    {
//        Debug.Log("WEBGL PREPROCESSOR - START");
//		string[] tiles = new string[] {"tile1", "tile2", "tile3", "tile4", "tile5", "tile6", "Wind"};
//		foreach(var theme in Directory.GetDirectories("Assets/Resources/Themes"))
//		{
//			foreach (var tile in tiles)
//			{
//				ConfigSmall(string.Format("{0}/{1}.png", theme, tile));
//			}
//		}
		// foreach (var tile in tiles)
		// {
		// 	ConfigSmall(string.Format("Assets/Resources/Themes/Cats/{0}.png", tile));
        //     ConfigSmall(string.Format("Assets/Resources/Themes/Chess/{0}.png", tile));
		// }
		// ConfigSmall("Assets/Graphics/Sun_Icon.png");
//		Debug.Log("WEBGL PREPROCESSOR - END");
    }

    private static void ConfigSmall(string assetPath)
	{
		TextureImporter importer = AssetImporter.GetAtPath(assetPath) as TextureImporter;
		if (importer != null)
		{
			importer.filterMode = FilterMode.Bilinear;
			var settings = importer.GetPlatformTextureSettings("WebGL");
			settings.maxTextureSize = 128;
			settings.overridden = true;
			importer.SetPlatformTextureSettings(settings);
			importer.SaveAndReimport();
			Debug.Log("preprocessed (UCB): " + assetPath);
		}
	}

}
#endif