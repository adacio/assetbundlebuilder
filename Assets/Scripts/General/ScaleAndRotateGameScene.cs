﻿using System.Collections;
using UnityEngine;

public class ScaleAndRotateGameScene : MonoBehaviour 
{
	[SerializeField] private GameObject		_boardParentPortrait;
	[SerializeField] private GameObject		_boardFrame;
	[SerializeField] private GameObject		_rackParentPortrait;
	[SerializeField] private GameObject		_rackObject;

    void AdjustForExtraTallScreens()
    {
        // Add 80px of top padding for iPhoneX and other similarly tall screens
        if (Screen.width * 2 < Screen.height)
        {
            var rectTransform = GetComponent<RectTransform>();
            rectTransform.offsetMax = new Vector2(rectTransform.offsetMax.x, -80f);
        }
    }

	void OnEnable()
	{
		AdjustForExtraTallScreens();

		DeviceChange.OnOrientationChange += MyOrientationChangeCode;
		DeviceChange.OnResolutionChange += MyResolutionChangeCode;
		UpdateCameraSettings();
	}

	void OnDisable()
	{
		DeviceChange.OnOrientationChange -= MyOrientationChangeCode;
		DeviceChange.OnResolutionChange -= MyResolutionChangeCode;
	}

	void MyOrientationChangeCode(DeviceOrientation orientation)
	{
		UpdateCameraSettings();
	}

	void MyResolutionChangeCode(Vector2 resolution)
	{
		UpdateCameraSettings();
	}

	public void UpdateCameraSettings()
	{
        StartCoroutine(ArrangeGameScreenObjects());
	}

	void OnWillRenderObject() 
	{
		UpdateCameraSettings();
	}

	private void ScaleToParentRect(GameObject target, GameObject parent) 
	{
		var scale = Vector3.one;
		var targetRect = target.GetComponent<RectTransform>().rect;
		var parentRect = parent.GetComponent<RectTransform>().rect;

        var newScale = System.Math.Max(
			System.Math.Min(
				parentRect.width / targetRect.width,
				parentRect.height / targetRect.height
			), 
			0.5f
		);

        scale.x = newScale;
		scale.y = newScale;
		target.transform.localScale = scale;
	}

	public IEnumerator ArrangeGameScreenObjects() 
	{
        yield return new WaitForEndOfFrame();
		
        // Board scales edge-to-edge in portrait
        ScaleToParentRect(_boardFrame, _boardParentPortrait);
		ScaleToParentRect(_rackObject, _rackParentPortrait);

        yield return null;
        yield return null;

    }
}
