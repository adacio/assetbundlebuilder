﻿#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class ThemeTester : MonoBehaviour
{
	[Header("Internal")]
	public GameObject tilePrefab;
	public Transform organizedLayoutParent;
	public Transform realLayoutParent;
	public Image bgA, bgB, bgC;

	public Image BoardBottomAdditiveImage;
	public Image BoardTopImage;
	public Image BoardTopFrameImage;
	public Image BoardTopFrameImageV2;
	public Image BoardGrid;
	public Image BoardCenterImage;
	public Image RackBottomAdditiveImage;
	public Image RackTopImage;
	public Image[] PlayerInfo;
	public Image ChatButtonImage;
	public Image RadialGradImage;
	public Tile[] DiscardedWinds;
	public Tile[] TilesOnRack;
	public FanfareTester Fanfare;

	private ThemeColors _colors;
	private Sprite _tileBaseDefault;
	private Sprite[] _tileSprites = new Sprite[7]; //6 base shapes + wind
	private Sprite[] _backgrounds = new Sprite[3];
	private Tile[,] _tiles = new Tile[6, 6];
	private List<BaseTileSprites> _baseTileSprites = new List<BaseTileSprites>();
	
	private const string _defaultBoardLayout = "0;51.63.31;51.62.31;31.38.19;32.41.20;0;20.24.12;0;41.51.25;52.64.32;11.15.7;11.14.7;01.3.1;02.5.2;03.7.3;00.1.0;10.13.6;40.49.24;0;13.19.9;12.17.8;02.4.2;04.8.4;05.11.5;00.0.0;0;0;0;13.18.9;14.20.10;04.9.4;54.68.34;0;30.36.18;35.47.23;25.35.17;55.71.35;15.22.11;12.16.8;0;53.67.33;23.31.15;33.42.21;35.46.23;15.23.11;54.69.34;55.70.35;0;0;53.66.33;33.43.21;30.37.18;0;45.59.29;0;05.10.5;0;0;43.55.27;23.30.15;20.25.12;40.48.24;44.56.28;34.44.22;0;01.2.1;0;0;22.29.14;42.52.26;43.54.27;41.50.25;24.32.16;0;0;0;0;0;0;44.57.28;42.53.26;";

	private List<Tile>[] _realLayoutTiles = new List<Tile>[6];

	[Header("---- Change this ----")]
	public string themeName;
	public string realBoardLayout;

	private void OnEnable()
	{
		ThemeColors.ColorsUpdatedHandler += OnColorUpdated;
	}

	private void OnDisable()
	{
		ThemeColors.ColorsUpdatedHandler -= OnColorUpdated;
	}

	private void OnColorUpdated()
	{
		RefreshColors();
	}

	private void Start()
	{
		Init();

		realLayoutParent.gameObject.SetActive(false);
	}

	private void Init()
	{
		for (int i = 0; i < 6; ++i)
			_realLayoutTiles[i] = new List<Tile>();

		LoadThemeData();
		FillBackground();
		CreateRealBoard();
		CreateTiles();
		RefreshColors();
	}

	private string _loadedTheme;
	public void Reload()
	{
		if (_loadedTheme != themeName)
		{
			_realLayoutTiles = new List<Tile>[6];
			_tiles = new Tile[6, 6];

			for (int i = 0; i < _tilesList.Count; i++)
			{
				DestroyImmediate(_tilesList[i]);
			}

			for (int i = 0; i < TilesOnRack.Length; ++i)
				TilesOnRack[i].ResetTile();
			for (int i = 0; i < DiscardedWinds.Length; ++i)
				DiscardedWinds[i].ResetTile();

			Init();
		}
	}

	public void LoadThemeData()
	{
		_colors = AssetDatabase.LoadAssetAtPath<ThemeColors>(string.Format("Assets/Themes/{0}/{1}", themeName, "Colors.asset"));
		Fanfare.InitColors(_colors);
		for (int i = 0; i < 6; ++i)
			_tileSprites[i] = AssetDatabase.LoadAssetAtPath<Sprite>(string.Format("Assets/Themes/{0}/tile{1}.png", themeName, i + 1));
		_tileSprites[6] = AssetDatabase.LoadAssetAtPath<Sprite>(string.Format("Assets/Themes/{0}/Wind.png", themeName));
		
		_tileBaseDefault = AssetDatabase.LoadAssetAtPath<Sprite>(string.Format("Assets/Themes/{0}/Base.png", themeName));

		_backgrounds[0] = AssetDatabase.LoadAssetAtPath<Sprite>(string.Format("Assets/Themes/{0}/A.png", themeName));
		_backgrounds[1] = AssetDatabase.LoadAssetAtPath<Sprite>(string.Format("Assets/Themes/{0}/B.png", themeName));
		_backgrounds[2] = AssetDatabase.LoadAssetAtPath<Sprite>(string.Format("Assets/Themes/{0}/C.png", themeName));

		LoadAllTileBaseLayers(_colors.BaseLayersData);

		InitializeDiscardedWindTiles();
		InitializeTilesOnRack();

		Fanfare.InitBackground(_backgrounds[0], _backgrounds[1], _backgrounds[2]);

		_loadedTheme = themeName;
	}

	private void LoadAllTileBaseLayers(List<ThemeColors.BaseLayerData> baseTilesData)
	{
		_baseTileSprites = new List<BaseTileSprites>();
		for (int i = 0; i < baseTilesData.Count; ++i)
		{
			_baseTileSprites.Add(new BaseTileSprites());
			if(baseTilesData[i].LayerSpriteBasedOnColor)
			{
				LoadBaseLayerSpriteVariations(ref _baseTileSprites[i].colorSprites[0], baseTilesData[i].Color1Variations);
				LoadBaseLayerSpriteVariations(ref _baseTileSprites[i].colorSprites[1], baseTilesData[i].Color2Variations);
				LoadBaseLayerSpriteVariations(ref _baseTileSprites[i].colorSprites[2], baseTilesData[i].Color3Variations);
				LoadBaseLayerSpriteVariations(ref _baseTileSprites[i].colorSprites[3], baseTilesData[i].Color4Variations);
				LoadBaseLayerSpriteVariations(ref _baseTileSprites[i].colorSprites[4], baseTilesData[i].Color5Variations);
				LoadBaseLayerSpriteVariations(ref _baseTileSprites[i].colorSprites[5], baseTilesData[i].Color6Variations);
				LoadBaseLayerSpriteVariations(ref _baseTileSprites[i].colorSprites[6], baseTilesData[i].ColorWindVariations);
			}
			if (baseTilesData[i].LayerSpriteBasedOnShape)
			{
				LoadBaseLayerSpriteVariations(ref _baseTileSprites[i].shapeSprites[0], baseTilesData[i].Shape1Variations);
				LoadBaseLayerSpriteVariations(ref _baseTileSprites[i].shapeSprites[1], baseTilesData[i].Shape2Variations);
				LoadBaseLayerSpriteVariations(ref _baseTileSprites[i].shapeSprites[2], baseTilesData[i].Shape3Variations);
				LoadBaseLayerSpriteVariations(ref _baseTileSprites[i].shapeSprites[3], baseTilesData[i].Shape4Variations);
				LoadBaseLayerSpriteVariations(ref _baseTileSprites[i].shapeSprites[4], baseTilesData[i].Shape5Variations);
				LoadBaseLayerSpriteVariations(ref _baseTileSprites[i].shapeSprites[5], baseTilesData[i].Shape6Variations);
				LoadBaseLayerSpriteVariations(ref _baseTileSprites[i].shapeSprites[6], baseTilesData[i].ShapeWindVariations);
			}

			LoadBaseLayerSpriteVariations(ref _baseTileSprites[i].defaultSprites, baseTilesData[i].DefaultLayerVariations);
			
		}
	}

	private void LoadBaseLayerSpriteVariations(ref List<Sprite> baseLayersList, List<string> listOfLayerVariations)
	{
		for(int i = 0; i < listOfLayerVariations.Count; ++i)
		{
			baseLayersList.Add(AssetDatabase.LoadAssetAtPath<Sprite>(string.Format("Assets/Themes/{0}/{1}.png", themeName, listOfLayerVariations[i])));
		}
	}

	private void FillBackground()
	{
		bgA.sprite = _backgrounds[0];
		bgB.sprite = _backgrounds[1];
		bgC.sprite = _backgrounds[2];
		bgA.preserveAspect = true;
		bgB.preserveAspect = true;
		bgC.preserveAspect = true;
	}

	private List<GameObject> _tilesList = new List<GameObject>();
	public void CreateTiles()
	{
		for (int x = 0; x < 6; ++x)
		{
			for (int y = 0; y < 6; ++y)
			{
				GameObject obj = Instantiate(tilePrefab, organizedLayoutParent);
				Tile tile = obj.GetComponent<Tile>();
				tile.SetTileSprites(x, _tileSprites, y, _baseTileSprites, _tileBaseDefault, _colors.BaseLayersData);
				tile.SetTileSprites(x, _tileSprites, y, _baseTileSprites, _tileBaseDefault, _colors.BaseLayersData);
				tile.RefreshTileColors(x, _colors.GetColorByNumber(y), y, _colors.IconLayersData, _colors._baseColor, _colors.BaseLayersData);
				_tiles[x, y] = tile;
				_tilesList.Add(obj);
			}
		}
	}

	public void CreateRealBoard()
	{
		string[] deserializedTiles = realBoardLayout.Split(';'); //e.g. 32.40.50

		for (int i = 0; i < deserializedTiles.Length - 1; ++i)
		{
			GameObject obj = Instantiate(tilePrefab, realLayoutParent);
			Tile tile = obj.GetComponent<Tile>();
			if (deserializedTiles[i] != "0")
			{
				string[] str = deserializedTiles[i].Split('.'); //So we have an array of: 32 and 40 and 50
				int Color = int.Parse(str[0][0].ToString()); //So we get 3
				int Shape = int.Parse(str[0][1].ToString()); //We get 2

				tile.SetTileSprites(Shape, _tileSprites, Color,  _baseTileSprites, _tileBaseDefault, _colors.BaseLayersData);
				tile.RefreshTileColors(Shape, _colors.GetColorByNumber(Color),  Color, _colors.IconLayersData, _colors._baseColor, _colors.BaseLayersData);
				
				_realLayoutTiles[Color].Add(tile); //Sort them by color so when designer changes the color we can update only that one color
			}
			else
				tile.HideTile();

			_tilesList.Add(obj);
		}
	}

	public void InitializeDiscardedWindTiles()
	{
		for (int i = 0; i < DiscardedWinds.Length; ++i)
		{
			DiscardedWinds[i].SetTileSprites(6, _tileSprites, 6, _baseTileSprites, _tileBaseDefault, _colors.BaseLayersData);
			DiscardedWinds[i].RefreshTileColors(6, _colors._wind, 6, _colors.IconLayersData, _colors._baseColor, _colors.BaseLayersData);
		}
	}

	public void InitializeTilesOnRack()
	{
		TilesOnRack[0].SetTileSprites(6, _tileSprites, 6, _baseTileSprites, _tileBaseDefault, _colors.BaseLayersData);
		TilesOnRack[0].RefreshTileColors(6, _colors._wind, 6, _colors.IconLayersData, _colors._baseColor, _colors.BaseLayersData);

		for (int i = 1; i < TilesOnRack.Length; ++i)
		{
			int randomizedColorNumber = TilesOnRack[i].RandomizeColorForYourself();
			int randomizedShapeNumber = TilesOnRack[i].RandomizeShapeForYourself();
			TilesOnRack[i].SetTileSprites(randomizedShapeNumber, _tileSprites, randomizedColorNumber, _baseTileSprites, _tileBaseDefault, _colors.BaseLayersData);
			TilesOnRack[i].RefreshTileColors(randomizedShapeNumber, _colors.GetColorByNumber(randomizedColorNumber), randomizedColorNumber, _colors.IconLayersData, _colors._baseColor, _colors.BaseLayersData);
		}
	}

	public void RefreshColors()
	{
		_colors = AssetDatabase.LoadAssetAtPath<ThemeColors>(string.Format("Assets/Themes/{0}/{1}", themeName, "Colors.asset"));
		Fanfare.InitColors(_colors);
		if (organizedLayoutParent.gameObject.activeSelf)
		{
			for (int x = 0; x < 6; ++x)
			{
				for (int y = 0; y < 6; ++y)
				{
					if(_tiles[x,y] != null)
						_tiles[x, y].RefreshTileColors(x, _colors.GetColorByNumber(y), y, _colors.IconLayersData, _colors._baseColor, _colors.BaseLayersData);
				}
			}
		}

		if (realLayoutParent.gameObject.activeSelf)
		{
			for (int color = 0; color < 6; ++color)
			{
				int count = _realLayoutTiles[color].Count;
				for (int i = 0; i < count; ++i)
				{
					var tile = _realLayoutTiles[color][i];
					tile.RefreshTileColors(tile.MyShapeNumber, _colors.GetColorByNumber(tile.MyColorNumber), tile.MyColorNumber, _colors.IconLayersData, _colors._baseColor, _colors.BaseLayersData);
				}
			}
		}

		for (int i = 0; i < DiscardedWinds.Length; ++i)
		{
			DiscardedWinds[i].RefreshTileColors(6, _colors._wind, 6, _colors.IconLayersData, _colors._baseColor, _colors.BaseLayersData);
		}

		TilesOnRack[0].RefreshTileColors(6, _colors._wind, 6, _colors.IconLayersData, _colors._baseColor, _colors.BaseLayersData);//Always wind tile
		for (int i = 1; i < TilesOnRack.Length; ++i)
		{
			TilesOnRack[i].RefreshTileColors(TilesOnRack[i].MyShapeNumber, _colors.GetColorByNumber(TilesOnRack[i].MyColorNumber), TilesOnRack[i].MyColorNumber, _colors.IconLayersData, _colors._baseColor, _colors.BaseLayersData);
		}
		
		BoardBottomAdditiveImage.color = _colors.BoardBottomAdditive;
		BoardTopImage.color = _colors.BoardTop;
		BoardTopFrameImage.color = _colors.BoardFrame;
		BoardTopFrameImageV2.color = _colors.BoardFrameV2;
		BoardGrid.color = _colors.BoardGrid;
		BoardCenterImage.color = _colors.MoonSquare;
		RackBottomAdditiveImage.color = _colors.RackBottomAdditive;
		RackTopImage.color = _colors.RackTop;
		PlayerInfo[0].color = _colors.PlayerInfoBox;
		PlayerInfo[1].color = _colors.PlayerInfoBox;
		ChatButtonImage.color = _colors.ChatButton;
		RadialGradImage.color = _colors.RadialGrad;
	}

	public void ToggleLayout()
	{
		organizedLayoutParent.gameObject.SetActive(!organizedLayoutParent.gameObject.activeSelf);
		realLayoutParent.gameObject.SetActive(!realLayoutParent.gameObject.activeSelf);
		RefreshColors();
	}

	public void RestoreDefaultTintColor()
	{
		var configFile = File.OpenText(Path.Combine(Application.dataPath, "defaultTint.json"));
		var config = JsonUtility.FromJson<ThemeColors.TintColorSetup>(configFile.ReadToEnd());

		_colors.RackTop = config.RackTop;
		_colors.RackBottomAdditive = config.RackBottomAdditive;
		_colors.BoardTop = config.BoardTop;
		_colors.BoardBottomAdditive = config.BoardBottomAdditive;
		_colors.PlayerInfoBox = config.PlayerInfoBox;
		_colors.BoardFrame = config.BoardFrame;
		_colors.BoardFrameV2 = config.BoardFrameV2;
		_colors.BoardGrid = config.BoardGrid;
		_colors.ChatButton = config.ChatButton;
		_colors.MoonSquare = config.MoonSquare;
		_colors.RadialGrad = config.RadialGrad;

		configFile.Close();
	}

	public void RestoreDefaultBoardString()
	{
		realBoardLayout = _defaultBoardLayout;
	}
}

#endif