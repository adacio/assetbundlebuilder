﻿using UnityEngine;
using UnityEngine.UI;

public class FanfareTester : MonoBehaviour {
	
	[SerializeField] private Image _bgA;
	[SerializeField] private Image _bgB;
	[SerializeField] private Image _bgC;
	[SerializeField] private Image _bgDim;
	[SerializeField] private Animator _myAnimator;
	
	public void InitBackground(Sprite A, Sprite B, Sprite C)
	{
		_bgA.sprite = A;
		_bgB.sprite = B;
		_bgC.sprite = C;
	}

	public void InitColors(ThemeColors colors)
	{
		_bgDim.color = colors.FanfareTint;
	}

	public void StartAnimation()
	{
		_myAnimator.Play("FanfareAnimation");
	}
}
