﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ThemeTester))]
public class ThemeTesterEditorExt : Editor
{
	ThemeTester _myScript;

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		_myScript = (ThemeTester)target;

		if (Application.isPlaying)
		{
			if (GUILayout.Button("Reload theme"))
			{
				_myScript.Reload();
			}
		}
		if (GUILayout.Button("Restore default tint color"))
		{
			_myScript.RestoreDefaultTintColor();
		}

		if (GUILayout.Button("Restore default board string"))
		{
			_myScript.RestoreDefaultBoardString();
		}
	}
}
