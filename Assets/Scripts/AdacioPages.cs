﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdacioPages : MonoBehaviour {

    [SerializeField] private float TimeOnPage;
    [SerializeField] private float PageChangeSpeed;

    [SerializeField] private List<RectTransform> _pages;

    private int _currentPage;
    private int _screenWidth;
    private float _containerHeight;

    private void Awake()
    {
        _screenWidth = Screen.width;
        _containerHeight = GetComponent<RectTransform>().sizeDelta.y;
    }

    private void OnEnable()
    {
        Init();
        StartAutoScroll();
    }

    private void Init()
    {
        for (int i = 1; i < _pages.Count; ++i)
            _pages[i].gameObject.SetActive(false);
        _currentPage = 0;
    }

    private void StartAutoScroll()
    {
        StartCoroutine(StartAutoScrollImpl());
    }

    private IEnumerator StartAutoScrollImpl()
    {
        while(true)
        {
            yield return new WaitForSeconds(TimeOnPage);
            StartCoroutine(NextPageImpl());
        }
    }

    private void NextPage()
    {
        StartCoroutine(NextPageImpl());
    }

    private IEnumerator NextPageImpl()
    {
        int currentPageIndex = _currentPage;
        int nextPageIndex = GetNextPageIndex();
        _pages[nextPageIndex].gameObject.SetActive(true);
        _pages[nextPageIndex].anchoredPosition = new Vector2(1024, 0);
        LeanTween.moveLocalX(_pages[currentPageIndex].gameObject, -1024, PageChangeSpeed);
        LeanTween.moveLocalX(_pages[nextPageIndex].gameObject, 0, PageChangeSpeed);
        yield return new WaitForSeconds(PageChangeSpeed);
        _pages[currentPageIndex].gameObject.SetActive(false);

    }

    private int GetNextPageIndex()
    {
        if (_currentPage == _pages.Count - 1)
        {
            _currentPage = 0;
            return _currentPage;
        }
        else return ++_currentPage;
    }
}
