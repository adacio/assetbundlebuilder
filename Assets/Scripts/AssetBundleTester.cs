﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class AssetBundleTester : MonoBehaviour {

	private Image _testImage;
	private AssetBundle _bundle;

	public string assetBundleName;
	public string imageName;

	// Use this for initialization
	void Start () {
		_testImage = GetComponent<Image>();
		LoadAssetBundle();
		_testImage.sprite = _bundle.LoadAsset<Sprite>(imageName);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void LoadAssetBundle()
	{
		_bundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, assetBundleName));
	}
}
