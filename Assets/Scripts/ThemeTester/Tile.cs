﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tile : MonoBehaviour 
{
	public Image icon;
	public List<Image> _baseLayers;
	
	public int MyColorNumber { get; private set; }
	public int MyShapeNumber { get; private set; }

	public void SetTileSprites(int iconShapeNumber, Sprite[] tileSprites, int iconColorNumber, List<BaseTileSprites> baseLayersSprites, Sprite defaultBase, List<ThemeColors.BaseLayerData> baseLayersData)
	{
		icon.sprite = tileSprites[iconShapeNumber];
		icon.preserveAspect = true;

		MyColorNumber = iconColorNumber;
		MyShapeNumber = iconShapeNumber;
		
		if (baseLayersData.Count > 0) //New systen with layers
		{ 
			for (int i = 0; i < baseLayersData.Count; ++i)
			{
				_baseLayers[i].gameObject.SetActive(true);

				List<Sprite> colorSpriteList = baseLayersSprites[i].colorSprites[iconColorNumber];
				List<Sprite> shapeSpriteList = baseLayersSprites[i].shapeSprites[iconShapeNumber];
				List<Sprite> defaultSpriteList = baseLayersSprites[i].defaultSprites;

				if (baseLayersData[i].LayerSpriteBasedOnColor)
				{
					if(colorSpriteList.Count > 0)
						_baseLayers[i].sprite = colorSpriteList[Random.Range(0, colorSpriteList.Count)];
					else
					{
						if (defaultSpriteList.Count > 0) //Fallback to default
							_baseLayers[i].sprite = defaultSpriteList[Random.Range(0, defaultSpriteList.Count)];
						else _baseLayers[i].gameObject.SetActive(false); //No sprites provided for this variation. Hide layer
					}
				}
				else if (baseLayersData[i].LayerSpriteBasedOnShape)
				{
					if(shapeSpriteList.Count > 0)
						_baseLayers[i].sprite = shapeSpriteList[Random.Range(0, shapeSpriteList.Count)];
					else
					{
						if(defaultSpriteList.Count > 0)
							_baseLayers[i].sprite = defaultSpriteList[Random.Range(0, defaultSpriteList.Count)];
						else _baseLayers[i].gameObject.SetActive(false); //No sprites provided for this variation. Hide layer
					}
				}
				else
				{
					if(defaultSpriteList.Count > 0)
						_baseLayers[i].sprite = defaultSpriteList[Random.Range(0, defaultSpriteList.Count)];
					else _baseLayers[i].gameObject.SetActive(false); //No sprites provided for this variation. Hide layer
				}
				
				if(baseLayersData[i].RandomBaseRotation)
				{
					int copyRotationFromLayer = baseLayersData[i].CopyRotationFromLayer;
					if (baseLayersData[i].CopyBaseRotation && copyRotationFromLayer < i) //We can only copy rotation from lower layers
						_baseLayers[i].transform.localRotation = _baseLayers[copyRotationFromLayer].transform.localRotation;
					else
						_baseLayers[i].transform.Rotate(0, 0, 90 * Random.Range(0, 4));
				}

				float scaleX = 1;
				float scaleY = 1;

				if(baseLayersData[i].RandomBaseXMirror)
				{
					var copyXMirrorLayer = baseLayersData[i].CopyXMirrorFromLayer;
					if (baseLayersData[i].CopyBaseXMirror && copyXMirrorLayer < i)
						scaleX = _baseLayers[i].transform.localScale.x;
					else
					{
						int rand = Random.Range(0, 2);
						if (rand == 0)
							scaleX = -1;
						else scaleX = 1;
					}
				}

				if (baseLayersData[i].RandomBaseYMirror)
				{
					var copyYMirrorLayer = baseLayersData[i].CopyYMirrorFromLayer;
					if (baseLayersData[i].CopyBaseYMirror && copyYMirrorLayer < i)
						scaleY = _baseLayers[i].transform.localScale.y;
					else
					{
						int rand = Random.Range(0, 2);
						if (rand == 0)
							scaleY = -1;
						else scaleY = 1;
					}
				}

				_baseLayers[i].transform.localScale = new Vector3(scaleX, scaleY, 1);
			}
		}
		else //Backward-compatibility mode. No layers
		{
			_baseLayers[0].sprite = defaultBase;
		}
	}
	
	public void RefreshTileColors(int iconShapeNumber, Color iconColor, int colorNumber, List<ThemeColors.IconLayerData> iconLayersData, Color defaultBaseColor, List<ThemeColors.BaseLayerData> baseLayersData)
	{
		icon.color = iconColor;

		if (iconLayersData.Count > 0)
		{
			switch (iconShapeNumber)
			{
				//We're using 0 index for iconLayersData because we do not support multiple icon layers yet. Change that to "for" loop later.
				case 0: icon.transform.localScale = new Vector3(iconLayersData[0].Shape1IconScale, iconLayersData[0].Shape1IconScale, 1); break;
				case 1: icon.transform.localScale = new Vector3(iconLayersData[0].Shape2IconScale, iconLayersData[0].Shape2IconScale, 1); break;
				case 2: icon.transform.localScale = new Vector3(iconLayersData[0].Shape3IconScale, iconLayersData[0].Shape3IconScale, 1); break;
				case 3: icon.transform.localScale = new Vector3(iconLayersData[0].Shape4IconScale, iconLayersData[0].Shape4IconScale, 1); break;
				case 4: icon.transform.localScale = new Vector3(iconLayersData[0].Shape5IconScale, iconLayersData[0].Shape5IconScale, 1); break;
				case 5: icon.transform.localScale = new Vector3(iconLayersData[0].Shape6IconScale, iconLayersData[0].Shape6IconScale, 1); break;
				case 6: icon.transform.localScale = new Vector3(iconLayersData[0].ShapeWindIconScale, iconLayersData[0].ShapeWindIconScale, 1); break;
			}
		}
		else
		{
			icon.transform.localScale = new Vector3(0.8f, 0.8f, 1f);
		}

		if (baseLayersData.Count > 0)
		{
			for (int i = 0; i < baseLayersData.Count; ++i)
			{
				if(baseLayersData[i].LayerColorBasedOnIconColor)
				{
					switch(colorNumber)
					{
						case 0: _baseLayers[i].color = baseLayersData[i].LayerColorIfColor1; break;
						case 1: _baseLayers[i].color = baseLayersData[i].LayerColorIfColor2; break;
						case 2: _baseLayers[i].color = baseLayersData[i].LayerColorIfColor3; break;
						case 3: _baseLayers[i].color = baseLayersData[i].LayerColorIfColor4; break;
						case 4: _baseLayers[i].color = baseLayersData[i].LayerColorIfColor5; break;
						case 5: _baseLayers[i].color = baseLayersData[i].LayerColorIfColor6; break;
						case 6: _baseLayers[i].color = baseLayersData[i].LayerColorIfWind; break;
					}
				}
				else
					_baseLayers[i].color = baseLayersData[i].LayerColor;
			}
		}
		else
			_baseLayers[0].color = defaultBaseColor;

	}

	public void HideTile()
	{
		icon.sprite = null;
		icon.color = Color.clear;
		_baseLayers[0].color = Color.clear;
		_baseLayers[1].color = Color.clear;
	}

	public int RandomizeColorForYourself()
	{
		MyColorNumber = Random.Range(0, 6);
		return MyColorNumber;
	}

	public int RandomizeShapeForYourself()
	{
		MyShapeNumber = Random.Range(0, 6);
		return MyShapeNumber;
	}

	public void ResetTile()
	{
		for(int i = 0; i < _baseLayers.Count; ++i)
		{
			_baseLayers[i].transform.localScale.Set(1, 1, 1);
			_baseLayers[i].transform.localRotation = Quaternion.identity;
		}
		
	}
}
