﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;


public class PromoBannerOpenLink : MonoBehaviour {

    //"https://www.facebook.com/laticegame/"
    //https://twitter.com/laticegame/
    //http://blog.latice.com/i-love-latice-valentines-day-giveaway/

#if UNITY_WEBGL
	[DllImport("__Internal")]
	public static extern void OpenWindow(string url);
#endif

    public void OpenURL(string url)
    {
#if !UNITY_WEBGL || UNITY_EDITOR
        Application.OpenURL(url);
#endif
#if UNITY_WEBGL && !UNITY_EDITOR
        OpenWindow(url);
#endif
    }


    private string GetPromoUrl()
    {
        string promoURL = "http://amzn.to/2w3kPT6";
        string CC = "US";//UserManager.Instance.CountryCode;
        if (!string.IsNullOrEmpty(CC))
        {
            switch (CC)
            {
                case "CA":
                    promoURL = "http://amzn.to/2wS1kea";
                    break;
                case "DE":
                    promoURL = "https://www.amazon.de/s/ref=nb_sb_noss_2?url=search-alias%3Daps&field-keywords=latice+game";
                    break;
                case "GB":
                    promoURL = "https://www.amazon.co.uk/s/ref=nb_sb_noss_2?url=search-alias%3Daps&field-keywords=latice+game";
                    break;
                case "FR":
                    promoURL = "https://www.amazon.fr/s/ref=nb_sb_noss_2?url=search-alias%3Daps&field-keywords=latice+game";
                    break;
                case "ES":
                    promoURL = "https://www.amazon.es/s/ref=nb_sb_noss_2?url=search-alias%3Daps&field-keywords=latice+game";
                    break;
                case "IT":
                    promoURL = "https://www.amazon.it/s/ref=nb_sb_noss_2?url=search-alias%3Daps&field-keywords=latice+game";
                    break;
                default:
                    promoURL = "http://amzn.to/2w3kPT6";
                    break;
            }
        }
        else Debug.LogError("CC is null");
        return promoURL;
    }

    public void OpenAmazonLink()
    {
#if !UNITY_WEBGL || UNITY_EDITOR
        Application.OpenURL(GetPromoUrl());
#endif
#if UNITY_WEBGL && !UNITY_EDITOR
        OpenWindow(GetPromoUrl());
#endif
    }


}
