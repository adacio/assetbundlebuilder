﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public delegate void EmptyEvent();

[CreateAssetMenu(fileName = "Colors", menuName = "ScriptableObjects/ThemeColorSet")]
public class ThemeColors : ScriptableObject
{
	public static EmptyEvent ColorsUpdatedHandler;
	//Default colors = Latice Classic colors.
	[SerializeField] public Color _red = new Color32(0xE4, 0x02, 0x00, 0xFF); //E40200FF
	[SerializeField] public Color _yellow = new Color32(0xFF, 0xDA, 0x00, 0xFF); //FFDA00FF
	[SerializeField] public Color _darkBlue = new Color32(0x00, 0x3A, 0xCB, 0xFF); //003ACBFF
	[SerializeField] public Color _green = new Color32(0x95, 0xF0, 0x00, 0xFF); //95F000FF
	[SerializeField] public Color _lightBlue = new Color32(0x93, 0xD0, 0xFD, 0xFF); //93D0FDFF
	[SerializeField] public Color _magenta = new Color32(0x9F, 0x42, 0xE5, 0xFF); //9F42E5FF
	[SerializeField] public Color _wind = new Color32(0x50, 0x50, 0x50, 0xFF); //505050FF
	[SerializeField] public Color _baseColor = Color.white;
	[SerializeField] public Color _rackInactiveTiles = new Color32(0x14, 0x14, 0x14, 0x50);

	[Header("UI Color tint")]
	[SerializeField] public Color RackTop = new Color(0.0f, 0.5186207294464111f, 1.0f, 0.7058823704719544f);
	[SerializeField] public Color RackBottomAdditive = new Color(0.0f, 0.5176470875740051f, 1.0f, 0.3921568691730499f);
	[SerializeField] public Color BoardTop = new Color(0.0f, 0.5559999942779541f, 1.0f, 1.0f);
	[SerializeField] public Color BoardBottomAdditive = new Color(0.0f, 0.5568627715110779f, 1.0f, 0.5882353186607361f);
	[SerializeField] public Color PlayerInfoBox = new Color(0.19215688109397889f, 0.5803921818733215f, 0.9411765336990356f, 1.0f);
	[SerializeField] public Color BoardFrame = new Color(0.0f, 0.5568627715110779f, 1.0f, 1.0f);
	[SerializeField] public Color BoardFrameV2 = Color.white;
	[SerializeField] public Color BoardGrid = Color.white;
	[SerializeField] public Color ChatButton = new Color(0.0f, 0.523676872253418f, 1.0f, 1.0f);
	[SerializeField] public Color MoonSquare = new Color(0.0f, 0.523676872253418f, 1.0f, 1.0f);
	[SerializeField] public Color RadialGrad = new Color(0.0f, 0.523676872253418f, 1.0f, 0.625f);

	[Header("Fanfare colors")]
	[SerializeField] public Color FanfareTint = Color.white;

	[Header("Tile Base settings")]
	public List<BaseLayerData> BaseLayersData = new List<BaseLayerData>();
	[Header("Tile Icon settings")]
	public List<IconLayerData> IconLayersData = new List<IconLayerData>();

#if UNITY_EDITOR
	public void OnValidate()
	{
		ExportToJson();
		OnColorsUpdated();
	}

	[ContextMenu("Export to JSON")]
	public void ExportToJson()
	{
		var config = string.Empty;
		config = JsonUtility.ToJson(this, true);

		var assetPath = UnityEditor.AssetDatabase.GetAssetPath(this);
		var path = Path.Combine(Path.GetDirectoryName(assetPath), "ColorsConfig.json");

		using (var sw = File.CreateText(path))
		{
			sw.Write(config);
		}
	}

	[ContextMenu("Import from JSON")]
	public void ImportFromJson()
	{
		var assetPath = UnityEditor.AssetDatabase.GetAssetPath(this);
		var path = Path.Combine(Path.GetDirectoryName(assetPath), "ColorsConfig.json");

		var config = JsonUtility.FromJson<ColorsConfig>(File.OpenText(path).ReadToEnd());
		_red = config._red;
		_baseColor = config._baseColor;
		_darkBlue = config._darkBlue;
		_green = config._green;
		_lightBlue = config._lightBlue;
		_magenta = config._magenta;
		_rackInactiveTiles = config._rackInactiveTiles;
		_wind = config._wind;
		_yellow = config._yellow;

		var tintConfig = JsonUtility.FromJson<TintColorSetup>(File.OpenText(path).ReadToEnd());
		RackTop = tintConfig.RackTop;
		RackBottomAdditive = tintConfig.BoardBottomAdditive;
		BoardTop = tintConfig.BoardTop;
		BoardBottomAdditive = tintConfig.BoardBottomAdditive;
		PlayerInfoBox = tintConfig.PlayerInfoBox;
		BoardFrame = tintConfig.BoardFrame;
		BoardFrameV2 = tintConfig.BoardFrameV2;
		BoardGrid = tintConfig.BoardGrid;
		ChatButton = tintConfig.ChatButton;
		MoonSquare = tintConfig.MoonSquare;
		RadialGrad = tintConfig.RadialGrad;
		FanfareTint = tintConfig.FanfareTint;

		BaseLayersData = new List<BaseLayerData>();
		var layersConfig = JsonUtility.FromJson<BaseLayersDataList>(File.OpenText(path).ReadToEnd());
		for (int i = 0; i < layersConfig.BaseLayersData.Count; ++i)
		{
			BaseLayersData.Add(new BaseLayerData());

			BaseLayersData[i].LayerColor = layersConfig.BaseLayersData[i].LayerColor;
			BaseLayersData[i].LayerColorBasedOnIconColor = layersConfig.BaseLayersData[i].LayerColorBasedOnIconColor;
			BaseLayersData[i].LayerColorIfColor1 = layersConfig.BaseLayersData[i].LayerColorIfColor1;
			BaseLayersData[i].LayerColorIfColor2 = layersConfig.BaseLayersData[i].LayerColorIfColor2;
			BaseLayersData[i].LayerColorIfColor3 = layersConfig.BaseLayersData[i].LayerColorIfColor3;
			BaseLayersData[i].LayerColorIfColor4 = layersConfig.BaseLayersData[i].LayerColorIfColor4;
			BaseLayersData[i].LayerColorIfColor5 = layersConfig.BaseLayersData[i].LayerColorIfColor5;
			BaseLayersData[i].LayerColorIfColor6 = layersConfig.BaseLayersData[i].LayerColorIfColor6;
			BaseLayersData[i].LayerColorIfWind = layersConfig.BaseLayersData[i].LayerColorIfWind;

			BaseLayersData[i].DefaultLayerVariations = layersConfig.BaseLayersData[i].DefaultLayerVariations;

			BaseLayersData[i].LayerSpriteBasedOnColor = layersConfig.BaseLayersData[i].LayerSpriteBasedOnColor;
			BaseLayersData[i].Color1Variations = layersConfig.BaseLayersData[i].Color1Variations;
			BaseLayersData[i].Color2Variations = layersConfig.BaseLayersData[i].Color2Variations;
			BaseLayersData[i].Color3Variations = layersConfig.BaseLayersData[i].Color3Variations;
			BaseLayersData[i].Color4Variations = layersConfig.BaseLayersData[i].Color4Variations;
			BaseLayersData[i].Color5Variations = layersConfig.BaseLayersData[i].Color5Variations;
			BaseLayersData[i].Color6Variations = layersConfig.BaseLayersData[i].Color6Variations;
			BaseLayersData[i].ColorWindVariations = layersConfig.BaseLayersData[i].ColorWindVariations;

			BaseLayersData[i].LayerSpriteBasedOnShape = layersConfig.BaseLayersData[i].LayerSpriteBasedOnShape;
			BaseLayersData[i].Shape1Variations = layersConfig.BaseLayersData[i].Shape1Variations;
			BaseLayersData[i].Shape2Variations = layersConfig.BaseLayersData[i].Shape2Variations;
			BaseLayersData[i].Shape3Variations = layersConfig.BaseLayersData[i].Shape3Variations;
			BaseLayersData[i].Shape4Variations = layersConfig.BaseLayersData[i].Shape4Variations;
			BaseLayersData[i].Shape5Variations = layersConfig.BaseLayersData[i].Shape5Variations;
			BaseLayersData[i].Shape6Variations = layersConfig.BaseLayersData[i].Shape6Variations;
			BaseLayersData[i].ShapeWindVariations = layersConfig.BaseLayersData[i].ShapeWindVariations;

			BaseLayersData[i].RandomBaseRotation = layersConfig.BaseLayersData[i].RandomBaseRotation;
			BaseLayersData[i].RandomBaseXMirror = layersConfig.BaseLayersData[i].RandomBaseXMirror;
			BaseLayersData[i].RandomBaseYMirror = layersConfig.BaseLayersData[i].RandomBaseYMirror;
			BaseLayersData[i].CopyRotationFromLayer = layersConfig.BaseLayersData[i].CopyRotationFromLayer;
			BaseLayersData[i].CopyXMirrorFromLayer = layersConfig.BaseLayersData[i].CopyXMirrorFromLayer;
			BaseLayersData[i].CopyYMirrorFromLayer = layersConfig.BaseLayersData[i].CopyYMirrorFromLayer;
		}

		IconLayersData = new List<IconLayerData>();
		var iconLayersConfig = JsonUtility.FromJson<IconLayersDataList>(File.OpenText(path).ReadToEnd());
		for(int i = 0; i < iconLayersConfig.IconLayersData.Count; ++i)
		{
			IconLayersData.Add(new IconLayerData());
			IconLayersData[i].Shape1IconScale = iconLayersConfig.IconLayersData[i].Shape1IconScale;
			IconLayersData[i].Shape2IconScale = iconLayersConfig.IconLayersData[i].Shape2IconScale;
			IconLayersData[i].Shape3IconScale = iconLayersConfig.IconLayersData[i].Shape3IconScale;
			IconLayersData[i].Shape4IconScale = iconLayersConfig.IconLayersData[i].Shape4IconScale;
			IconLayersData[i].Shape5IconScale = iconLayersConfig.IconLayersData[i].Shape5IconScale;
			IconLayersData[i].Shape6IconScale = iconLayersConfig.IconLayersData[i].Shape6IconScale;
		}
	}

	public Color GetColorByNumber(int i)
	{
		switch (i)
		{
			case 0:
				return _red;
			case 1:
				return _yellow;
			case 2:
				return _darkBlue;
			case 3:
				return _green;
			case 4:
				return _lightBlue;
			case 5:
				return _magenta;
			default:
				return _wind;
		}
	}

	private void OnColorsUpdated()
	{
		if (ColorsUpdatedHandler != null)
			ColorsUpdatedHandler();
	}
#endif

	[Serializable]
	private class ColorsConfig
	{
		[SerializeField] public Color _red;
		[SerializeField] public Color _yellow;
		[SerializeField] public Color _darkBlue;
		[SerializeField] public Color _green;
		[SerializeField] public Color _lightBlue;
		[SerializeField] public Color _magenta;
		[SerializeField] public Color _wind;
		[SerializeField] public Color _baseColor;
		[SerializeField] public Color _rackInactiveTiles;
	}

	[Serializable]
	public class TintColorSetup
	{
		[SerializeField] public Color RackTop;
		[SerializeField] public Color RackBottomAdditive;
		[SerializeField] public Color BoardTop;
		[SerializeField] public Color BoardBottomAdditive;
		[SerializeField] public Color PlayerInfoBox;
		[SerializeField] public Color BoardFrame;
		[SerializeField] public Color BoardFrameV2;
		[SerializeField] public Color BoardGrid;
		[SerializeField] public Color ChatButton;
		[SerializeField] public Color MoonSquare;
		[SerializeField] public Color RadialGrad;
		[SerializeField] public Color FanfareTint;
	}

	[Serializable]
	public class BaseLayersDataList
	{
		public List<BaseLayerData> BaseLayersData;
	}

	[Serializable]
	public class IconLayersDataList
	{
		public List<IconLayerData> IconLayersData;
	}

	[Serializable]
	public class BaseLayerData
	{
		[Header("Colors")]
		public Color LayerColor;
		public bool LayerColorBasedOnIconColor;
		public Color LayerColorIfColor1;
		public Color LayerColorIfColor2;
		public Color LayerColorIfColor3;
		public Color LayerColorIfColor4;
		public Color LayerColorIfColor5;
		public Color LayerColorIfColor6;
		public Color LayerColorIfWind;
		[Header("Rotations and mirroring")]
		public bool RandomBaseRotation;
		public bool RandomBaseXMirror;
		public bool RandomBaseYMirror;
		public bool CopyBaseRotation;
		public int CopyRotationFromLayer;
		public bool CopyBaseXMirror;
		public int CopyXMirrorFromLayer;
		public bool CopyBaseYMirror;
		public int CopyYMirrorFromLayer;
		[Header("Default sprite variations")]
		public List<string> DefaultLayerVariations;
		[Header("Sprites based on icon shape")]
		public bool LayerSpriteBasedOnShape;
		public List<string> Shape1Variations;
		public List<string> Shape2Variations;
		public List<string> Shape3Variations;
		public List<string> Shape4Variations;
		public List<string> Shape5Variations;
		public List<string> Shape6Variations;
		public List<string> ShapeWindVariations;
		[Header("Sprites based on icon color")]
		public bool LayerSpriteBasedOnColor;
		public List<string> Color1Variations;
		public List<string> Color2Variations;
		public List<string> Color3Variations;
		public List<string> Color4Variations;
		public List<string> Color5Variations;
		public List<string> Color6Variations;
		public List<string> ColorWindVariations;
	}

	[Serializable]
	public class IconLayerData
	{
		[Range(0, 1)] public float Shape1IconScale = 0.8f;
		[Range(0, 1)] public float Shape2IconScale = 0.8f;
		[Range(0, 1)] public float Shape3IconScale = 0.8f;
		[Range(0, 1)] public float Shape4IconScale = 0.8f;
		[Range(0, 1)] public float Shape5IconScale = 0.8f;
		[Range(0, 1)] public float Shape6IconScale = 0.8f;
		[Range(0, 1)] public float ShapeWindIconScale = 0.8f;
	}
}