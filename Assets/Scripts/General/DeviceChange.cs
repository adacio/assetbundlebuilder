﻿using System;
using System.Collections;
using UnityEngine;

public class DeviceChange : MonoBehaviour
{
	public static event Action<Vector2> OnResolutionChange;
	public static event Action<DeviceOrientation> OnOrientationChange;
	public static float CheckDelay = 0.25f;         // How long to wait until we check again.
	public static DeviceOrientation orientation;           // Current Device Orientation

	static Vector2 resolution;                      // Current Resolution
	bool isAlive = true;                     // Keep this script running?
	private WaitForSeconds _refreshDelay;

	void Start()
	{
		_refreshDelay = new WaitForSeconds(CheckDelay);
		StartCoroutine(CheckForChange());
	}

	public void CheckResolutionChange()
	{
#if UNITY_EDITOR
        if (Camera.main != null && (resolution.x != Screen.width * Camera.main.rect.width || resolution.y != Screen.height * Camera.main.rect.height))
        {
            resolution = new Vector2(Screen.width * Camera.main.rect.width, Screen.height * Camera.main.rect.height);
#else
		if (resolution.x != Screen.width || resolution.y != Screen.height)
		{
			resolution.x = Screen.width;
			resolution.y = Screen.height;
#endif
			if (OnResolutionChange != null)
			{
				OnResolutionChange.Invoke(resolution);
			}
		}
	}

	public IEnumerator CheckForChange()
	{
		resolution.x = Screen.width;
		resolution.y = Screen.height;

		orientation = Input.deviceOrientation;

		while (isAlive)
		{
			CheckResolutionChange();

			// Check for an Orientation Change
			switch (Input.deviceOrientation)
			{
				case DeviceOrientation.Unknown:            // Ignore
				case DeviceOrientation.FaceUp:            // Ignore
				case DeviceOrientation.FaceDown:        // Ignore
					break;
				default:
					if (orientation != Input.deviceOrientation)
					{
						orientation = Input.deviceOrientation;
						if (OnOrientationChange != null)
						{
							Debug.Log("Will call orientation change");
							OnOrientationChange(orientation);
						}
					}
					break;
			}

			yield return _refreshDelay;
		}
	}

	void OnDestroy()
	{
		isAlive = false;
	}
}